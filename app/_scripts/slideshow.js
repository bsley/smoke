var photoSet = [

"_img/slide_01.jpg", 
"_img/slide_02.jpg", 
"_img/slide_03.jpg"

];
var p = photoSet.length - 1;
var i = 0;
var z = 0;
var original = document.getElementById("item");
//console.log("total photos: " + photoSet.length);

function loopPhotos() {
  setTimeout(function() {
    var clone = original.cloneNode(true);
    clone.id = "item " + ++z;
    original.parentNode.appendChild(clone);
    $(clone).css({
      'background-image': 'url(' + photoSet[i] + ')',
    });
    $(clone).fadeIn(500);
    i++;
    // console.log("displaying photo: " + i + "/" + photoSet.length);
    if (i < p) {
      // call the next indexed image and begin to preload it
      var img = new Image();
      img.src = photoSet[i];
    } else if (i > p) {
      // reset the counter once the total photos have been displayed
      i = 0;
    }
    loopPhotos();
  }, 4000)
}
$(document).ready(function() {

  // load first image
  var firstImg = new Image();
  firstImg.src = photoSet[0];
  // when first image is loaded, fade it in and begin looping
  firstImg.onload = function() {
    //$('.myslideshow').fadeIn();
    loopPhotos();
    $('.bigLogo svg g').velocity("transition.fadeIn", {
      duration: 1000,
      stagger: 300,
      delay: 500,
    });
    $('nav').velocity("transition.slideDownIn", {
      duration: 800,
      delay: 5500,
    });
  };
});