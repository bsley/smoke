
// SCROLLTO FUNCTION OF TOP NAV LINKS

$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 200);
        return false;
      }
    }
  });
});

// "READ MORE" FUNCTIONALITY

$(document).on("click", '.ReadMoreLink', function(event) { 
  $(this).parent().find(".StylistDesc").toggleClass("open");
  console.log("read more!");
});

$(document).on("click", '.StylistDesc', function(event) { 
  $(this).toggleClass("open");
  console.log("read more!");
});

$(".logomark").click(function() {
  $("#skrollr-body").animate({ scrollTop: 0 }, 100);
  $("html, body").animate({ scrollTop: 0 }, 100);
  return false;
});

// LOADING STYLISTS

$(document).ready(myfunction);
$(window).on('resize',myfunction);

function myfunction() {
  if (document.documentElement.clientWidth < 770) {
    $( "#Stylists" ).load( "stylists_mobile.html" );
    //console.log("mobile stylists loaded!")
  } 
  else if (document.documentElement.clientWidth > 770) {
    $( "#Stylists" ).load( "stylists_desktop.html" );
    // load Skrollr only on larger screens
    var s = skrollr.init({forceHeight: false});
    //console.log("desktop stylists loaded!")
  };

};


